var isMayor = (req, res, next) => {
  var edat = req.body.age || '';
  var nom = req.body.name || '';
  var majorEdat = '';

  if (edat < 18){
    majorEdat = "no és major d'edat";
  } else {
    majorEdat = "és major d'edat";
  }
  
  res.send('<html><body><h1>' + nom + ', ' + majorEdat + '</h1></body></html>');  

};

var User = require('../models/user');

var newUser = (req, res, next) => {
  var user = new User({name: req.body.name, age: req.body.age});
  user.save();
  next();
};

module.exports = {isMayor, newUser};