const express = require('express');
const router = express.Router();
const path = require('path');
const UserController = require('../controller/UserController.js');

//Middleware para mostrar datos del request
router.use (function (req,res,next) {
  console.log('/' + req.method);
  next();
});

//Analiza la ruta y el protocolo y reacciona en consecuencia
router.get('/',function(req,res){
  res.sendFile(path.resolve('views/index.html'));
});

router.post('/mayor', UserController.newUser);
router.post('/mayor', UserController.isMayor);

module.exports = router;

